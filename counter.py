from machine import Pin
import time

idx = 0

led_DP = Pin("GP12", Pin.OUT)
led_D = Pin("GP13", Pin.OUT)
led_E = Pin("GP14", Pin.OUT)
led_C = Pin("GP15", Pin.OUT)
led_G = Pin("GP16", Pin.OUT)
led_F = Pin("GP17", Pin.OUT)
led_A = Pin("GP18", Pin.OUT)
led_B = Pin("GP19", Pin.OUT)

led_pins = [led_DP, led_D, led_E, led_C, led_G, led_F, led_A, led_B]
numbers = [0b11101110, 0b10001000, 0b11010110, 0b11011010, 0b10111000, 0b01111010, 0b01111110, 0b11001000, 0b11111110, 0b11111010]

button_fwd = Pin("GP20", Pin.IN, pull=Pin.PULL_DOWN)
button_bck = Pin("GP21", Pin.IN, pull=Pin.PULL_DOWN)

def set_leds(leds, mask):
    for led in leds:
        led.value(mask & 1)
        mask >>= 1
        
class ButtonHandler():
    def __init__(self,change=0):
        self.change = change
        self.last = 0
    
    def handler(self, pin):
        global idx
        pin.irq(None)
        time.sleep(0.02)
        if pin.value() == 1:
            idx += self.change
        pin.irq(self.handler, trigger=Pin.IRQ_RISING)


button_fwd.irq(ButtonHandler(+1).handler, trigger=Pin.IRQ_RISING)
button_bck.irq(ButtonHandler(-1).handler, trigger=Pin.IRQ_RISING)

for pin in led_pins:
    pin.value(0)

def run():
    while True:
        set_leds(led_pins, numbers[idx%len(numbers)])
        time.sleep(0.1)
